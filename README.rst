This repository contains the data and code for replicating the analysis presented in:

- Arthur A. B. Pessa, Rafael S. Zola, Matjaz Perc and Haroldo V. Ribeiro, `Determining liquid crystal properties with ordinal networks and machine learning <http://complex.pfi.uem.br/paper/2022/1-s2.0-S0960077921009619-main.pdf>`_, Chaos, Solitons & Fractals 154, 111607 (2022).

If you have used our data in a scientific publication, please cite the following reference:

.. code-block:: bibtex
    
   @article{pessa2022determining,
    title         = {Determining liquid crystal properties with ordinal networks and machine learning}, 
    author        = {Arthur A. B. Pessa, Rafael S. Zola, Matjaz Perc and Haroldo V. Ribeiro},
    journal       = {Chaos, Solitons \& Fractals},
    volume        = {154},
    number        = {6},
    pages         = {111607},
    doi           = {10.1016/j.chaos.2021.111607},
   }
